''''
convert.py

Converts XLSX file with single sheet from argv[1] to CSV file.
'''


import sys
import os
import csv
from openpyxl import load_workbook

chemin = str(sys.argv[1])

listeFichiers = os.listdir(chemin)
while listeFichiers != []:
    fichier = listeFichiers.pop()
    wf = chemin + fichier
    wb = load_workbook(filename = wf)
    ws = wb.active
    with open(''.join([wf,'.csv']), 'w') as f:
        wr = csv.writer(f, delimiter=';')
        for row in ws.iter_rows():
            lrow = []
            for cell in row:
                lrow.append(cell.value)
            wr.writerow(lrow)
    os.remove(wf)

